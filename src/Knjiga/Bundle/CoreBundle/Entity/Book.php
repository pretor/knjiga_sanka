<?php

namespace Knjiga\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Book
 */
class Book
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $product;

    private $user;

    /**
     * @var integer
     */
    private $prenetaKolicina;

    /**
     * @var integer
     */
    private $nabavnaKolicina;

    /**
     * @var integer
     */
    private $ukupnaKolicina;

    /**
     * @var integer
     */
    private $zaliheNaKrajuDana;

    /**
     * @var integer
     */
    private $utrosenaKolicina;

    /**
     * @var string
     */
    private $ostvareniPromet;

    /**
     * @var string
     */
    private $vrednostZaliha;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;
    /**
     * @var \DateTime
     */
    private $date;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param integer $product
     * @return Book
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return integer 
     */
    public function getProduct()
    {
        return $this->product;
    }
    /**
     * Set user
     *
     * @param integer $user
     * @return Book
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set prenetaKolicina
     *
     * @param integer $prenetaKolicina
     * @return Book
     */
    public function setPrenetaKolicina($prenetaKolicina)
    {
        $this->prenetaKolicina = $prenetaKolicina;

        return $this;
    }

    /**
     * Get prenetaKolicina
     *
     * @return integer 
     */
    public function getPrenetaKolicina()
    {
        return $this->prenetaKolicina;
    }

    /**
     * Set nabavnaKolicina
     *
     * @param integer $nabavnaKolicina
     * @return Book
     */
    public function setNabavnaKolicina($nabavnaKolicina)
    {
        $this->nabavnaKolicina = $nabavnaKolicina;

        return $this;
    }

    /**
     * Get nabavnaKolicina
     *
     * @return integer 
     */
    public function getNabavnaKolicina()
    {
        return $this->nabavnaKolicina;
    }

    /**
     * Set ukupnaKolicina
     *
     * @param integer $ukupnaKolicina
     * @return Book
     */
    public function setUkupnaKolicina($ukupnaKolicina)
    {
        $this->ukupnaKolicina = $ukupnaKolicina;

        return $this;
    }

    /**
     * Get ukupnaKolicina
     *
     * @return integer 
     */
    public function getUkupnaKolicina()
    {
        return $this->ukupnaKolicina;
    }

    /**
     * Set zaliheNaKrajuDana
     *
     * @param integer $zaliheNaKrajuDana
     * @return Book
     */
    public function setZaliheNaKrajuDana($zaliheNaKrajuDana)
    {
        $this->zaliheNaKrajuDana = $zaliheNaKrajuDana;

        return $this;
    }

    /**
     * Get zaliheNaKrajuDana
     *
     * @return integer 
     */
    public function getZaliheNaKrajuDana()
    {
        return $this->zaliheNaKrajuDana;
    }

    /**
     * Set utrosenaKolicina
     *
     * @param integer $utrosenaKolicina
     * @return Book
     */
    public function setUtrosenaKolicina($utrosenaKolicina)
    {
        $this->utrosenaKolicina = $utrosenaKolicina;

        return $this;
    }

    /**
     * Get utrosenaKolicina
     *
     * @return integer 
     */
    public function getUtrosenaKolicina()
    {
        return $this->utrosenaKolicina;
    }

    /**
     * Set ostvareniPromet
     *
     * @param string $ostvareniPromet
     * @return Book
     */
    public function setOstvareniPromet($ostvareniPromet)
    {
        $this->ostvareniPromet = $ostvareniPromet;

        return $this;
    }

    /**
     * Get ostvareniPromet
     *
     * @return string 
     */
    public function getOstvareniPromet()
    {
        return $this->ostvareniPromet;
    }

    /**
     * Set vrednostZaliha
     *
     * @param string $vrednostZaliha
     * @return Book
     */
    public function setVrednostZaliha($vrednostZaliha)
    {
        $this->vrednostZaliha = $vrednostZaliha;

        return $this;
    }

    /**
     * Get vrednostZaliha
     *
     * @return string 
     */
    public function getVrednostZaliha()
    {
        return $this->vrednostZaliha;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Book
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Book
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Book
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
