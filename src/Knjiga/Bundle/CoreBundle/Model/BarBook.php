<?php

namespace Knjiga\Bundle\CoreBundle\Model;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAware;
use Knjiga\Bundle\UserBundle\Entity\User;
use Knjiga\Bundle\CoreBundle\Entity\Book;


/**
 * Book
 */
class BarBook extends ContainerAware
{
    /**
     *
     * @var EntityManager
     */
    protected $em;

    protected $user;

    protected $date;

    public function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
    }

    public function setUser(User $user){
        $this->user = $user;
        return $this;
    }

    public function getUser(){
        return $this->user;
    }
    public function setDate($date){
        $this->date = $date;
        return $this;
    }

    public function getDate(){
        return $this->date;
    }

    public function create(){

        $bookExists = $this->getBookExistanceForDate();


        //ako ne postoji... onda probas da pokupis od proslog datuma
        //ako i to ne postoji onda odradis startBarBookFirstTime

        if($bookExists){
           // knjiga postoji, nista se ne desava

        }
        else{
            $oldBarBook = $this->em->getRepository('KnjigaCoreBundle:Book')->findAllProductsByUserAndLastDate($this->getUser());

            /*foreach ($products as $product) {
               // var_dump($product->getProduct()->getName());
                var_dump($product->getDate());
            }
            die;*/

            if(!empty($oldBarBook)){
                $this->openBarBookForDate($oldBarBook, $this->getDate());
            }


            else{
                $this->startBarBookFirstTime();
            }

        }

    }

    public function startBarBookFirstTime(){
        //prvi unos najpre (prvi put se pocinje knjiga sanka)
        //treba da pokupi sve proizvode i da ih ubaci u Book sa nulama sve ostalo
        $products = $this->em->getRepository('KnjigaProductBundle:Product')->findAllProductsByUser($this->getUser());

        foreach($products as $product){
            $book = new Book();
            $book->setProduct($product);
            $book->setNabavnaKolicina(0);
            $book->setOstvareniPromet(0);
            $book->setPrenetaKolicina(0);
            $book->setUkupnaKolicina(0);
            $book->setUtrosenaKolicina(0);
            $book->setVrednostZaliha(0);
            $book->setZaliheNaKrajuDana(0);
            $book->setUser($this->getUser());
            $book->setDate($this->getDate());
            $this->em->persist($book);
            $this->em->flush();
        }
    }

    public function openBarBookForDate($oldBarBook, $date){

        //treba da se vidi da li $oldBarBook ima iste proizvode kao iz tabele proizvodi
        //e sad kako to... da li da uzmem sve proizvod ID-jeve pa da uporedjujem 2 niza



        $oldBarBookWithNewProducts = $this->_addNewProducts($oldBarBook);
        $oldBarBookWithoutOldProducts = $this->_removeDeletedProducts($oldBarBook)['oldBarBookWithRemovedDeletedProducts'];
        $newProducts = $this->_removeDeletedProducts($oldBarBook)['newProducts'];

        foreach($oldBarBookWithoutOldProducts as $oldBarBookEntry){
            $book = new Book();

            foreach ($newProducts as $newProduct) {
                if($oldBarBookEntry->getProduct()->getProductNumber() == $newProduct->getProductNumber()){
                    $book->setProduct($newProduct);
                }

            }
            if($book->getProduct() === null){
                $book->setProduct($oldBarBookEntry->getProduct());
            }
            $book->setNabavnaKolicina(0);
            $book->setOstvareniPromet(0);
            $book->setPrenetaKolicina($oldBarBookEntry->getZaliheNaKrajuDana());
            $book->setUkupnaKolicina($oldBarBookEntry->getZaliheNaKrajuDana());
            $book->setUtrosenaKolicina(0);
            $book->setVrednostZaliha($oldBarBookEntry->getVrednostZaliha());
            $book->setZaliheNaKrajuDana($oldBarBookEntry->getZaliheNaKrajuDana());
            $book->setUser($this->getUser());
            $book->setDate($date);
            $this->em->persist($book);
            $this->em->flush();
        }

    }

    public function getBookExistanceForDate()
    {
        $products = $this->em->getRepository('KnjigaCoreBundle:Book')->findAllProductsByUserAndDate($this->getUser(), $this->getDate());

        switch (empty($products)) {
            case true:
                return false;
                break;
            case false:
                return true;
                break;
        }
    }

    /**
     * Adds new products to Bar book
     * @param $oldBarBook
     * @return mixed
     */
    private function _addNewProducts($oldBarBook){

        $difference = $this->_findDifference($oldBarBook);

        $newProducts = array_diff(
            $difference['arrayOfActiveProducts'],
            $difference['arrayOfProductsInLastBook']
        );

        if(!empty($newProducts)){

            foreach ($newProducts as $newProductId) {

                $product = $this->em->getRepository('KnjigaProductBundle:Product')->findOneById($newProductId);

                $productsWithSameNumber = $this->em->getRepository('KnjigaProductBundle:Product')->findBy(
                    array('productNumber'=>$product->getProductNumber()));

                if(count($productsWithSameNumber) == 1){
                    $book = new Book();
                    $book->setProduct($product);
                    $book->setNabavnaKolicina(0);
                    $book->setOstvareniPromet(0);
                    $book->setPrenetaKolicina(0);
                    $book->setUkupnaKolicina(0);
                    $book->setUtrosenaKolicina(0);
                    $book->setVrednostZaliha(0);
                    $book->setZaliheNaKrajuDana(0);
                    $book->setUser($this->getUser());
                    $book->setDate($this->date);
                    $this->em->persist($book);
                    $this->em->flush();
                }
            }
        }

        return $oldBarBook;
    }

    /**
     * Removes deleted products from Bar book
     * @param $oldBarBook
     * @return array
     */
    private function _removeDeletedProducts($oldBarBook){

        $difference = $this->_findDifference($oldBarBook);

        $deletedProducts = array_diff(
            $difference['arrayOfProductsInLastBook'],
            $difference['arrayOfActiveProducts']
        );

        $oldBarBookWithRemovedDeletedProducts = array();
        $newProducts = array();
        foreach ($oldBarBook as $bookEntry) {
            $deleted = false;
            foreach ($deletedProducts as $deletedProductId) {

                //ovde treba imati proveru da li je to sto je obrisano posle toga zamenjeno sa novim
                //tako sto vidis da li postoji taj product u novoj verziji i onda se on dodaje u barr book

                if($bookEntry->getProduct()->getId() == $deletedProductId ){
                    $deleted = true;
                }
            }
            $productsWithSameNumber = $this->em->getRepository('KnjigaProductBundle:Product')->findBy(
                array('productNumber'=>$bookEntry->getProduct()->getProductNumber()));


            if(count($productsWithSameNumber) > 1 && $deleted == true){

                //znaci da ima vise od jedne verzije
                //sada proveravamo da li taj koji imamo ima najveci ID

                if(end($productsWithSameNumber)->getId() != $bookEntry->getProduct()->getId()){

                    $deleted = false;

                    // zameni se product za taj Book
                   // $bookEntry->setProduct(end($productsWithSameNumber));
                    //array_push($newProducts, array($bookEntry->getProduct()->getId() => end($productsWithSameNumber)));
                    array_push($newProducts, end($productsWithSameNumber));
                }
            }

            if(!$deleted) $oldBarBookWithRemovedDeletedProducts[] = $bookEntry;

        }
        //return $oldBarBookWithRemovedDeletedProducts;
        return array(
            'oldBarBookWithRemovedDeletedProducts' => $oldBarBookWithRemovedDeletedProducts,
            'newProducts' => $newProducts
        );
    }

    /**
     * Return 2 arrays,
     * first array contains ID's of products in Last Bar book
     * second array contains ID's of active products
     * @param $oldBarBook
     * @return array
     */
    private function _findDifference($oldBarBook){

        $products = $this->em->getRepository('KnjigaProductBundle:Product')->findAllProductsByUser($this->getUser());

        $arrayOfProductsInLastBook = array();
        $arrayOfActiveProducts = array();
        foreach ($oldBarBook as $bookEntry){
            $arrayOfProductsInLastBook[] = $bookEntry->getProduct()->getId();
        }
        foreach ($products as $product) {
            $arrayOfActiveProducts[] = $product->getId();
        }

        return array(
            'arrayOfProductsInLastBook'=>$arrayOfProductsInLastBook,
            'arrayOfActiveProducts'=> $arrayOfActiveProducts
        );
    }
}
