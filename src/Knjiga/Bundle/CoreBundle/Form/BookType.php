<?php

namespace Knjiga\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product')
            ->add('prenetaKolicina')
            ->add('nabavnaKolicina')
            ->add('ukupnaKolicina')
            ->add('zaliheNaKrajuDana')
            ->add('utrosenaKolicina')
            ->add('ostvareniPromet')
            ->add('vrednostZaliha')
            ->add('created')
            ->add('updated')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Knjiga\Bundle\CoreBundle\Entity\Book'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'knjiga_bundle_corebundle_book';
    }
}
