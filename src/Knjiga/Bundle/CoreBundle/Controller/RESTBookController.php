<?php

namespace Knjiga\Bundle\CoreBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

/**
 * Class RESTBookController
 * @package Knjiga\Bundle\CoreBundle\Controller
 * @author pretor <3nity.pretor@gmail.com>
 */
class RESTBookController extends FOSRestController
{
    /**
     * Updates Nabavna Kolicina
     * @param $id
     * @param $value
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateNabavnaKolicinaAction($id, $value){

        $book = $this->getDoctrine()
            ->getRepository('KnjigaCoreBundle:Book')->findOneById($id);

        $em = $this->getDoctrine()->getManager();

        $book->setNabavnaKolicina($value);
        $em->persist($book);
        $em->flush();

        $view = $this->view($book, 200)
            ->setTemplate("KnjigaCoreBundle:Default:index.html.twig")
            ->setTemplateVar('book')
        ;

        return $this->handleView($view);
    }

    public function updateUtrosenaKolicinaAction($id, $value){

        $book = $this->getDoctrine()
            ->getRepository('KnjigaCoreBundle:Book')->findOneById($id);

        $em = $this->getDoctrine()->getManager();

        $book->setUtrosenaKolicina($value);
        $em->persist($book);
        $em->flush();

        $view = $this->view($book, 200)
            ->setTemplate("KnjigaCoreBundle:Default:index.html.twig")
            ->setTemplateVar('book')
        ;

        return $this->handleView($view);
    }
    public function updateProductAction($id, $nabavnaKolicina, $ukupnaKolicina, $zaliheNaKrajuDana, $utrosenaKolicina, $ostvareniPromet, $vrednostZaliha){
        $book = $this->getDoctrine()
            ->getRepository('KnjigaCoreBundle:Book')->findOneById($id);

        $em = $this->getDoctrine()->getManager();

        $book->setNabavnaKolicina($nabavnaKolicina);
        $book->setUkupnaKolicina($ukupnaKolicina);
        $book->setZaliheNaKrajuDana($zaliheNaKrajuDana);
        $book->setUtrosenaKolicina($utrosenaKolicina);
        $book->setOstvareniPromet($ostvareniPromet);
        $book->setVrednostZaliha($vrednostZaliha);
        $em->persist($book);
        $em->flush();

        $view = $this->view($book, 200)
            ->setTemplate("KnjigaCoreBundle:Default:index.html.twig")
            ->setTemplateVar('book')
        ;

        return $this->handleView($view);
    }

}