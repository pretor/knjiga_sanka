<?php

namespace Knjiga\Bundle\CoreBundle\Controller;

use Knjiga\Bundle\CoreBundle\Model\BarBook;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Knjiga\Bundle\CoreBundle\Entity\Book;
use Knjiga\Bundle\CoreBundle\Form\BookType;

/**
 * Class BookController
 * @package Knjiga\Bundle\CoreBundle\Controller
 * @author pretor <3nity.pretor@gmail.com>
 */
class BookController extends Controller
{

    /**
     * Lists all Book entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $date = $this->_generateDate();

        //????
        $entities = $em->getRepository('KnjigaCoreBundle:Book')->findAllProductsByUserAndDate($this->getUser(), $date);

        function cmp($a, $b)
        {
            return ($a->getProduct()->getProductNumber() < $b->getProduct()->getProductNumber()) ? -1 : 1;
        }

        usort($entities, array($this, "cmp"));


        $form = $this->_generateForm($date);

        return $this->render('KnjigaCoreBundle:Book:index.html.twig', array(
            'entities' => $entities,
            'form'     => $form->createView(),
            'date'     => $date
        ));
    }
    public function openAction(){

        $date = $this->_generateDate();

        $BarBook = $this->container->get('bar.book');
        $BarBook->setUser($this->getUser());
        $BarBook->setDate($date);
        $BarBook->create();

        return $this->redirect($this->generateUrl('book',
            array('form'=>array(
                'date'=>array(
                    'year'=>$date->format('Y'),
                    'month'=>$date->format('m'),
                    'day'=>$date->format('d')
                )))));
    }
    /**
     * Creates a new Book entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Book();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('book_show', array('id' => $entity->getId())));
        }

        return $this->render('KnjigaCoreBundle:Book:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Book entity.
     *
     * @param Book $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Book $entity)
    {
        $form = $this->createForm(new BookType(), $entity, array(
            'action' => $this->generateUrl('book_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Book entity.
     *
     */
    public function newAction()
    {
        $entity = new Book();
        $form   = $this->createCreateForm($entity);

        return $this->render('KnjigaCoreBundle:Book:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Book entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KnjigaCoreBundle:Book')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Book entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('KnjigaCoreBundle:Book:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Book entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KnjigaCoreBundle:Book')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Book entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('KnjigaCoreBundle:Book:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Book entity.
    *
    * @param Book $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Book $entity)
    {
        $form = $this->createForm(new BookType(), $entity, array(
            'action' => $this->generateUrl('book_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Book entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KnjigaCoreBundle:Book')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Book entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('book_edit', array('id' => $id)));
        }

        return $this->render('KnjigaCoreBundle:Book:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Book entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('KnjigaCoreBundle:Book')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Book entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('book'));
    }

    /**
     * Creates a form to delete a Book entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('book_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * @return \DateTime
     */
    private function _generateDate(){
        if($_GET){
            $date = new \DateTime();
            $date->setDate($_GET['form']['date']['year'], $_GET['form']['date']['month'], $_GET['form']['date']['day']);
        }
        else{
            $date = new \DateTime("now");
        }
        return $date;
    }

    /**
     * @param \DateTime $date
     * @return \Symfony\Component\Form\Form
     */
    private function _generateForm(\DateTime $date){
        $defaultData = array('message' => 'Type your message here');

        return $this->createFormBuilder($defaultData)
            ->setAction($this->generateUrl('book_open'))
            ->setMethod('GET')
            ->add('date', 'date', array(
                'data' => $date
            ))

            ->add('Kreiraj za datum', 'submit')
            ->getForm();
    }
    function cmp($a, $b)
    {
        return ($a->getProduct()->getProductNumber() < $b->getProduct()->getProductNumber()) ? -1 : 1;
    }

}
