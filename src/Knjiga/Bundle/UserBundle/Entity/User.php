<?php
// src/Acme/UserBundle/Entity/User.php

namespace Knjiga\Bundle\UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;


class User extends BaseUser
{



    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}