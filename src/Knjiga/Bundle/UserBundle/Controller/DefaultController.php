<?php

namespace Knjiga\Bundle\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('KnjigaUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
