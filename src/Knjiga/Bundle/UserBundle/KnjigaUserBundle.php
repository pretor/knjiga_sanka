<?php

namespace Knjiga\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class KnjigaUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
