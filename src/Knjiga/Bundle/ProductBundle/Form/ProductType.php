<?php

namespace Knjiga\Bundle\ProductBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       // if($options['user'] == 'pretor') $options['user'] = null;

        $builder
            ->add('productNumber')
            ->add('name')
            ->add('price')
            ->add('taxRate')
            ->add('parent', 'entity', array(
                'class'    => 'KnjigaProductBundle:Product',
                'query_builder' => function(EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('q')
                        ->add('select', 'p')
                        ->from('Knjiga\Bundle\ProductBundle\Entity\Product', 'p')
                        ->where('p.active = true')
                        ->andHaving('p.user = :user')
                        ->setParameter(':user', $options['user']);
                },
                'property' => 'name',
                'multiple' => true,
                'placeholder' => 'Choose an option',
                'required'   => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Knjiga\Bundle\ProductBundle\Entity\Product',
            'user'       => null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'knjiga_bundle_productbundle_product';
    }
}
