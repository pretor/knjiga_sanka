<?php

namespace Knjiga\Bundle\ProductBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Knjiga\Bundle\ProductBundle\Entity\Product;
use Knjiga\Bundle\ProductBundle\Form\ProductType;

/**
 * Product controller.
 *
 */
class ProductController extends Controller
{

    /**
     * Lists all Product entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $entities = $em->getRepository('KnjigaProductBundle:Product')->findAllProductsByUser($user);

        return $this->render('KnjigaProductBundle:Product:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Product entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Product();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $user = $this->getUser();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($user);
            $entity->setActive(true);
            $em->persist($entity);

            $em->flush();

            return $this->redirect($this->generateUrl('product'));
        }

        return $this->render('KnjigaProductBundle:Product:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Product entity.
     *
     * @param Product $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Product $entity)
    {
        $form = $this->createForm(new ProductType(), $entity, array(
            'action' => $this->generateUrl('product_create'),
            'method' => 'POST',
            'user'   => $this->getUser()
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Product entity.
     *
     */
    public function newAction()
    {
        $entity = new Product();
        $form   = $this->createCreateForm($entity);

        return $this->render('KnjigaProductBundle:Product:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Product entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KnjigaProductBundle:Product')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('KnjigaProductBundle:Product:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KnjigaProductBundle:Product')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $editForm = $this->createEditForm($entity, $id);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('KnjigaProductBundle:Product:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Product entity.
    *
    * @param Product $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Product $entity, $id)
    {
        $form = $this->createForm(new ProductType(), $entity, array(
            'action' => $this->generateUrl('product_update', array('id' => $id)),
            'method' => 'PUT',
            'user'   => $this->getUser()
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Product entity.
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KnjigaProductBundle:Product')->find($id);

        $newProduct = new Product();

        $newProduct->setName($entity->getName());
        $newProduct->setActive(true);
        $newProduct->setPrice($entity->getPrice());
        $newProduct->setTaxRate($entity->getTaxRate());
        $newProduct->setUser($entity->getUser());
        $newProduct->setProductNumber($entity->getProductNumber());
        //$newProduct->setParent($entity->getParent());




        //ako ima decu, onda treba foreach za svako dete da se setuje novi parent ako je active dete
        foreach($entity->getBasicIngredients() as $child) {
            if($child->getActive()){

                $child->setActive(false);

                $newChildProduct = new Product();

                $newChildProduct->setName($child->getName());
                $newChildProduct->setActive(true);
                $newChildProduct->setPrice($child->getPrice());
                $newChildProduct->setTaxRate($child->getTaxRate());
                $newChildProduct->setUser($child->getUser());
                $newChildProduct->setProductNumber($child->getProductNumber());
                $newChildProduct->setParent($newProduct);

                $em->persist($newChildProduct);               

            }
        }



        foreach ($entity->getParent() as $parent) {

            $parent->setActive(false);
            
            $newParentProduct = new Product();
            //$newProduct->setParent($newParentProduct);

            //$child = new ArrayCollection();
            //$child->add($newProduct);

            $newParentProduct->setName($parent->getName());
            $newParentProduct->setActive(true);
            $newParentProduct->setPrice($parent->getPrice());
            $newParentProduct->setTaxRate($parent->getTaxRate());
            $newParentProduct->setUser($parent->getUser());
            $newParentProduct->setProductNumber($parent->getProductNumber());
            //$newParentProduct->setParent($parent->getParent());
            $newParentProduct->setBasicIngredients($newProduct);
            //$newProduct->removeParent($newParentProduct);
            $newProduct->setParent($newParentProduct);

            foreach($parent->getBasicIngredients() as $child) {
                if($child->getActive()){

                    $child->setActive(false);

                    $newChildProduct = new Product();

                    $newChildProduct->setName($child->getName());
                    $newChildProduct->setActive(true);
                    $newChildProduct->setPrice($child->getPrice());
                    $newChildProduct->setTaxRate($child->getTaxRate());
                    $newChildProduct->setUser($child->getUser());
                    $newChildProduct->setProductNumber($child->getProductNumber());
                    $newChildProduct->setParent($newParentProduct);

                    $em->persist($newChildProduct);

                    //a sta ako oni imaju decu ? rekurzija ?

                }
            }


            $em->persist($newParentProduct);

        }


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity, $id);
        $editForm->handleRequest($request);

      /*  foreach ($newProduct->getParent() as $parent) {


            var_dump($parent->getId());


        }
        die;*/

         if ($editForm->isValid()) {
            //treba da se uzme taj proizvod objekat i da se napravi novi, sa tim izmenama
            // onda bi trebalo i da se naprave nove veze parent deca ?!?!? ili ne ?!?!

            $entity->setActive(false);
           /*  foreach ($newProduct->getParent() as $parent) {


            var_dump($parent->getId());


             }
            die;*/
             $em->persist($newProduct);

            $em->flush();

            return $this->redirect($this->generateUrl('product_edit', array('id' => $id)));
        }

        return $this->render('KnjigaProductBundle:Product:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Product entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('KnjigaProductBundle:Product')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Product entity.');
            }

            $entity->setActive(false);
            $em->flush();


        return $this->redirect($this->generateUrl('product'));
    }

    /**
     * Creates a form to delete a Product entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('product_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
