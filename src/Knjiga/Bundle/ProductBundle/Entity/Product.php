<?php

namespace Knjiga\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Product
 */
class Product
{

    public function __construct()
    {
        $this->basicIngredients = new ArrayCollection();
        $this->parent = new ArrayCollection();
    }
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $productNumber;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $price;

    /**
     * @var string
     */
    private $taxRate;

    /**
     * @var array
     */
    private $basicIngredients;

    /**
     * @var integer
     */
    private $user;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var boolean
     */
    private $active;


    private $parent;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productNumber
     *
     * @param integer $productNumber
     * @return Product
     */
    public function setProductNumber($productNumber)
    {
        $this->productNumber = $productNumber;

        return $this;
    }

    /**
     * Get productNumber
     *
     * @return integer 
     */
    public function getProductNumber()
    {
        return $this->productNumber;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set taxRate
     *
     * @param string $taxRate
     * @return Product
     */
    public function setTaxRate($taxRate)
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    /**
     * Get taxRate
     *
     * @return string 
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }

    /**
     * Set basicIngredients
     *
     * @param  $basicIngredients
     * @return Product
     */
    public function setBasicIngredients($basicIngredients)
    {
        $this->basicIngredients[] = $basicIngredients;

        return $this;
    }

    /**
     * Get basicIngredients
     *
     * @return array 
     */
    public function getBasicIngredients()
    {
        return $this->basicIngredients;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return Product
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Product
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Product
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Product
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get parent
     * @return ArrayCollection
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set Parent
     * @param $parent
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent[] = $parent;
        return $this;
    }

    /**
     * Removes parent
     * @param $parent
     * @return bool
     */
    public function removeParent($parent)
    {
        return $this->parent->removeElement($parent);
    }

}
