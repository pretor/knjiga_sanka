<?php

namespace Knjiga\Bundle\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('KnjigaWebBundle:Default:index.html.twig');
    }
}
