$(function() {
    onLoadSracunajOstvareniPromet();
    onLoadSracunajZaliheNaKrajuDana();

    sracunajUkupneZalihenaPocetkuDana();
    sracunajUkupneZalihenNaKrajuDana();


    $( ".nabavna-kolicina" ).change(function() {
        //console.log($('#'+this.id).parent().parent().parent().attr('class'));
        // Ako nema roditelja onda se ovo dogadja
        if($('#'+this.id).parent().parent().parent().attr('class') == 'noParent'){

            var idArray = this.id.split('_');
            var redniBroj = idArray[2];

            var prenetaKolicina = parseInt($('#preneta-kolicina-'+redniBroj).html());

            var nabavnaKolicina = parseInt($('#nabavna_kolicina_'+redniBroj).val());

            var ukupnaKolicina = prenetaKolicina + nabavnaKolicina;

            var cena = parseInt($('#cena-'+redniBroj).html());

            var utrosenaKolicina = parseInt($('#utrosena_kolicina_'+redniBroj).val());

            var zaliheNaKrajuDana = ukupnaKolicina - utrosenaKolicina;
            var vrednostZaliha = ukupnaKolicina * cena;
            var ostvareniPromet = cena * utrosenaKolicina;

            $('#ukupna-kolicina-'+redniBroj).html(ukupnaKolicina);
            $('#vrednost-zaliha-'+redniBroj).html(vrednostZaliha);
            $('#zalihe-na-kraju-dana-'+redniBroj).html(zaliheNaKrajuDana);

            var idBook = $('#'+this.id).parent().parent().parent().attr('id');


            var field = '#nabavna-progress-';
            updateProduct(idBook, nabavnaKolicina, ukupnaKolicina, zaliheNaKrajuDana , utrosenaKolicina, ostvareniPromet, vrednostZaliha, field );

        }
        // A ako ima roditelja onda ovo:
        else{

        }
        sracunajUkupneZalihenaPocetkuDana();
        sracunajUkupneZalihenNaKrajuDana();



    });

    $( ".utrosena-kolicina" ).change(function() {
        // Ako nema roditelja onda se ovo dogadja
        if($('#'+this.id).parent().parent().parent().attr('class') == 'noParent'){
            var idArray = this.id.split('_');
            var redniBroj = idArray[2];

            //treba da se napravi provera da ako roditelj ima decu onda da ubaci u racunanje i njihove vrednosti

            var cena = parseInt($('#cena-'+redniBroj).html());

            var utrosenaKolicina = parseInt($('#utrosena_kolicina_'+redniBroj).val());

            var ukupnaKolicina = parseInt($('#ukupna-kolicina-'+redniBroj).html());

            var ostvareniPromet = cena * utrosenaKolicina;
            $('#ostvareni-promet-'+redniBroj).html(parseInt(ostvareniPromet) + parseInt(sracunajDeca(redniBroj).ukupnaOstvarenaVrednost));

            $('#zalihe-na-kraju-dana-'+redniBroj).html(ukupnaKolicina - utrosenaKolicina);
            $('#zalihe_na_kraju_dana_'+redniBroj).val(ukupnaKolicina - utrosenaKolicina);

            var nabavnaKolicina = parseInt($('#nabavna_kolicina_'+redniBroj).val());
            var zaliheNaKrajuDana = ukupnaKolicina - utrosenaKolicina;
            var vrednostZaliha = ukupnaKolicina * cena;
            var field = '#utrosena-progress-';

            var idBook = $('#'+this.id).parent().parent().parent().attr('id');

            updateProduct(idBook, nabavnaKolicina, ukupnaKolicina, zaliheNaKrajuDana , utrosenaKolicina, ostvareniPromet, vrednostZaliha, field );
        }
        // A ako ima roditelja onda ovo:
        else
        {
            var roditeljId = $('#'+this.id).parent().parent().parent().attr('class');

           // console.log('roditeljID je: '+roditeljId);

            var idArray = this.id.split('_');
            var redniBroj = idArray[2];

           // console.log('redni broj je je: '+redniBroj);

            var cena = parseInt($('#cena-'+roditeljId).html());

           // console.log('cena roditelja: '+cena);

            var utrosenaKolicina = parseInt($('#utrosena_kolicina_'+roditeljId).val());

            var ostvareniPrometRoditelja = cena * utrosenaKolicina;

            $('#ostvareni-promet-'+roditeljId).html(parseInt(ostvareniPrometRoditelja) + parseInt(sracunajDeca(roditeljId).ukupnaOstvarenaVrednost));

            var utrosenaKolicina = parseInt($('#utrosena_kolicina_'+roditeljId).val());

            var ukupnaKolicina = parseInt($('#ukupna-kolicina-'+roditeljId).html());

            $('#zalihe-na-kraju-dana-'+roditeljId).html((ukupnaKolicina - utrosenaKolicina) - sracunajDeca(roditeljId).utrosenaKolicina);
            $('#zalihe_na_kraju_dana_'+roditeljId).val((ukupnaKolicina - utrosenaKolicina) - sracunajDeca(roditeljId).utrosenaKolicina);

            var utroseno = parseInt($('#utrosena_kolicina_'+redniBroj).val());
            var idBook = $('#'+this.id).parent().parent().parent().attr('id');

            updateUtrosenaKolicina(idBook, utroseno);

            var idBookParent = $('#utrosena_kolicina_' + roditeljId).parent().parent().parent().attr('id');
            var field = '#utrosena-progress-';
            var nabavnaKolicina = parseInt($('#nabavna_kolicina_'+roditeljId).val());
            var zaliheNaKrajuDana = ukupnaKolicina - utrosenaKolicina;
            var ostvareniPromet = cena * utrosenaKolicina;
            var vrednostZaliha = ukupnaKolicina * cena;

            updateProduct(idBookParent, nabavnaKolicina, ukupnaKolicina, zaliheNaKrajuDana , utrosenaKolicina, ostvareniPromet, vrednostZaliha, field );

        }
        sracunajUkupneZalihenaPocetkuDana();
        sracunajUkupneZalihenNaKrajuDana();
        //updateUtrosenaKolicina(redniBroj, utrosenaKolicina);
       /* var nabavnaKolicina = parseInt($('#nabavna_kolicina_'+redniBroj).val());
        var zaliheNaKrajuDana = ukupnaKolicina - utrosenaKolicina;
        var vrednostZaliha = ukupnaKolicina * cena;
        var field = '#utrosena-progress-';
        // updateProduct(redniBroj, nabavnaKolicina, ukupnaKolicina, zaliheNaKrajuDana , utrosenaKolicina, ostvareniPromet, vrednostZaliha );
        updateProduct(redniBroj, nabavnaKolicina, ukupnaKolicina, zaliheNaKrajuDana , utrosenaKolicina, 6, 6, field );*/

    });

    function sracunajUkupneZalihenaPocetkuDana()
    {
        var zalihe = 0;
        jQuery('.vrednost-zaliha').each(function() {
            var currentElement = $(this);
            var value = parseInt(currentElement.html());
            zalihe=zalihe+value;
        });

        $('#pocetak-dana').html(zalihe);
        $('#svega').html(zalihe - parseInt($('#kraj-dana').html()));
    }
    function sracunajUkupneZalihenNaKrajuDana()
    {
        var zalihe = 0;
        jQuery('.ostvareni-promet').each(function() {
            var currentElement = $(this);
            var value = parseInt(currentElement.html());
            zalihe=zalihe+value;
        });

        $('#kraj-dana').html(zalihe);
        $('#svega').html(parseInt($('#pocetak-dana').html()) - zalihe);
    }
    function onLoadSracunajOstvareniPromet()
    {
        jQuery('.ostvareni-promet').each(function() {
            var idArray = this.id.split('-');
            var redniBroj = idArray[2];

            var prenetaKolicina = parseInt($('#preneta-kolicina-'+redniBroj).html());

            var nabavnaKolicina = parseInt($('#nabavna_kolicina_'+redniBroj).val());

            var ukupnaKolicina = prenetaKolicina + nabavnaKolicina;

            var cena = parseInt($('#cena-'+redniBroj).html());

            $('#ukupna-kolicina-'+redniBroj).html(ukupnaKolicina);
            $('#vrednost-zaliha-'+redniBroj).html(ukupnaKolicina * cena);
        });
    }
    function onLoadSracunajZaliheNaKrajuDana()
    {
        jQuery('.vrednost-zaliha').each(function() {
            // Ako nema roditelja onda se ovo dogadja
            if($('#'+this.id).parent().attr('class') == 'noParent'){
                var idArray = this.id.split('-');
                var redniBroj = idArray[2];

                var cena = parseInt($('#cena-'+redniBroj).html());

                var utrosenaKolicina = parseInt($('#utrosena_kolicina_'+redniBroj).val());

                var ukupnaKolicina = parseInt($('#ukupna-kolicina-'+redniBroj).html());

                $('#ostvareni-promet-'+redniBroj).html(parseInt(cena * utrosenaKolicina) + parseInt(sracunajDeca(redniBroj).ukupnaOstvarenaVrednost));

                console.log(cena * utrosenaKolicina);

                $('#zalihe-na-kraju-dana-'+redniBroj).html(ukupnaKolicina - utrosenaKolicina);
                $('#zalihe_na_kraju_dana_'+redniBroj).val(ukupnaKolicina - utrosenaKolicina);
            }
            // A ako ima roditelja onda ovo:
            else
            {
                var roditeljId = $('#'+this.id).parent().attr('class');

                var idArray = this.id.split('-');
                var redniBroj = idArray[2];

                var cena = parseInt($('#cena-'+redniBroj).html());

                var utrosenaKolicina = parseInt($('#utrosena_kolicina_'+redniBroj).val());

                var ostvareniPrometRoditelja = parseInt($('#utrosena_kolicina_'+roditeljId).val()) * parseInt($('#cena-'+roditeljId).html());


                var zaliheNakrajuDanaRoditelja = parseInt($('#zalihe-na-kraju-dana-'+roditeljId).html());

                $('#ostvareni-promet-'+roditeljId).html(ostvareniPrometRoditelja + parseInt(sracunajDeca(roditeljId).ukupnaOstvarenaVrednost));

                $('#zalihe-na-kraju-dana-'+roditeljId).html(zaliheNakrajuDanaRoditelja - utrosenaKolicina);
                $('#zalihe_na_kraju_dana_'+redniBroj).val(zaliheNakrajuDanaRoditelja - utrosenaKolicina);

            }

            sracunajUkupneZalihenaPocetkuDana();
            sracunajUkupneZalihenNaKrajuDana();
        });
    }

    /**
     * Dobija parametar ID roditelja i vraca koliko sva deca imaju utrosenu kulicinu i vrednost ukupnu
     * @param roditeljId
     * @returns {string}
     */
    function sracunajDeca(roditeljId){
        var ukupnaUtrosenaKolicina = '0';
        var ukupnaOstvarenaVrednost = '0'

        if($("."+roditeljId)){
        jQuery("."+roditeljId).each(function() {

            var idArray = this.children[3].id.split('-');
            var redniBroj = idArray[2];

            var cena = parseInt($('#cena-'+redniBroj).html());

            var deteUtrosenaKolicina = parseInt($('#utrosena_kolicina_'+redniBroj).val());

            ukupnaOstvarenaVrednost = parseInt(ukupnaOstvarenaVrednost) + (deteUtrosenaKolicina * cena);

            ukupnaUtrosenaKolicina = parseInt(ukupnaUtrosenaKolicina) + deteUtrosenaKolicina;

        })
        }

        var result = {
            utrosenaKolicina : ukupnaUtrosenaKolicina,
            ukupnaOstvarenaVrednost : ukupnaOstvarenaVrednost
        };

        return result;
    }
    function updateUtrosenaKolicina(id, utrosenaKolicina){
        var progressField = $('#utrosena-progress-'+id)
        progressField.progress({ percent: 1 });

        $.post( "/book_rest/utrosenakolicina/"+ id +"/"+utrosenaKolicina, function() {
            progressField.progress({ percent: 50 });
        })
            .done(function() {
                progressField.progress({ percent: 100 });
            })
            .fail(function() {
                alert( "error" );
            })
    }
    //path:     /updateproduct/{id}/{nabavnaKolicina}/{ukupnaKolicina}/{zaliheNaKrajuDana}/{utrosenaKolicina}/{ostvareniPromet}/{vrednostZaliha}

    function updateProduct(id, nabavnaKolicina, ukupnaKolicina, zaliheNaKrajuDana, utrosenaKolicina, ostvareniPromet, vrednostZaliha, field ){
        var progressField= $(field+id);
        progressField.progress({ percent: 1 });
        $.post(
            "/book_rest/updateproduct/"+ id +"/"+nabavnaKolicina+"/"+ukupnaKolicina+"/"+zaliheNaKrajuDana+"/"+utrosenaKolicina+"/"+ostvareniPromet+"/"+vrednostZaliha
            , function(data) {
                progressField.progress({ percent: 50 });
                console.log(data);
        })
            .done(function() {
                progressField.progress({ percent: 100 });
            })
            .fail(function() {
                alert( "error" );
            })
    }

    /**
     * Enable only numeric and dot on input fields
     */
    $("input.nabavna-kolicina, input.utrosena-kolicina").numeric(".");

    $('.ui.dropdown')
        .dropdown()
    ;



});